export type DayProps = {
    name: string;
    type: string;
    temperature: number;
    isToday: boolean;
    dayMonthName?: string;
    humidity?: number;
    rain_probability?: number;
}
