import React from 'react';
import forecastService from './services/forecastService';
import { Filter } from './components/Filter';
import { Head } from './components/Head';
import { CurrentWeather } from './components/CurrentWeather';
import { Forecast } from './components/Forecast';

export const App: React.FC = () => (
    <main>
        <Filter />
        <Head forecastService={forecastService} />
        <CurrentWeather forecastService={forecastService} />
        <Forecast forecastService={forecastService} />
    </main>
);
