import React from 'react';
import { ForecastServiceBase } from '../services/forecastService';

type HeadProps = {
    forecastService: ForecastServiceBase
};
export const Head: React.FC<HeadProps> = ({ forecastService }) => {
    const current = forecastService.getTodayForecast();
    
    return(
        <div className="head">
            <div className={`icon ${current.type}`}></div>
            <div className="current-date">
                <p>{current.name}</p>
                <span>{current.dayMonthName}</span>
            </div>
        </div>
    )
};
