import React from 'react';
import { ForecastServiceBase } from '../services/forecastService';
import { Day } from './Day';

type ForecastProps = {
    forecastService: ForecastServiceBase
};
 
export const Forecast: React.FC<ForecastProps> = ({ forecastService }) => {
    const days = forecastService.getWeekForecast()
        .map((day) => {
            return <Day
                name={day.name}
                temperature={day.temperature}
                type={day.type}
                isToday={day.isToday} />;
        });

    return (
        <div className="forecast">
            {days}
        </div>
    );
};
