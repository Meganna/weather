import React from 'react';
import { DayProps } from '../types/dayPropsType';

export const Day: React.FC<DayProps> = (dayProps) => (
    <div className={`day ${dayProps.type} ${dayProps.isToday ? 'selected': ''}`}>
        <p>{dayProps.name}</p>
        <span>{dayProps.temperature}</span>
    </div>
);
