import React from 'react';
import { ForecastServiceBase } from '../services/forecastService';

type CurrentWeatherProps = {
    forecastService: ForecastServiceBase
};

export const CurrentWeather: React.FC<CurrentWeatherProps> = ({ forecastService }) => {
    const current = forecastService.getTodayForecast();

    return (
        <div className="current-weather">
        <p className="temperature">{current.temperature}</p>
        <p className="meta">
            <span className="rainy">%{current.rain_probability}</span>
            <span className="humidity">%{current.humidity}</span>
        </p>
    </div>
    )
};
