import forecastData from "../mock-data/forecast.json";
import { DayProps } from '../types/dayPropsType';

export interface ForecastServiceBase{
    getWeekForecast(): Array<DayProps>;
    getTodayForecast(): DayProps;
};

class ForecastService implements ForecastServiceBase {
    getWeekForecast() {
        return forecastData
            .slice(0, 7)
            .map((dayData, i) => {
                const day = new Date(dayData.day);
                return {
                    name: day.toLocaleString('ru-ru', {weekday: 'long'}),
                    dayMonthName: day.toLocaleString('ru-ru', {day: 'numeric', month: 'long'}),
                    type: dayData.type,
                    temperature: dayData.temperature,
                    isToday: i === 0,
                    humidity: dayData.humidity,
                    rain_probability: dayData.rain_probability
                }
            });
    };

    getTodayForecast() {
        const day = new Date(forecastData[0].day);
        return {
            name: day.toLocaleString('ru-ru', {weekday: 'long'}),
            dayMonthName: day.toLocaleString('ru-ru', {day: 'numeric', month: 'long'}),
            type: forecastData[0].type,
            temperature: forecastData[0].temperature,
            isToday: true,
            humidity: forecastData[0].humidity,
            rain_probability: forecastData[0].rain_probability
        }
    }
}

export default new ForecastService();
